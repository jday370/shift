package environment;

import java.util.ArrayList;

import objects.Platform;

public class Environment {
	public static final double GRAVITY = 1; // must be divisible by .5 (e.g, 1.5, 2, 2.5, 3, 3.5, ..)

	public static ArrayList<Platform> envObjects = new ArrayList<Platform>();

	public static void moveAllY(int velocity) {
		for (Platform obj : envObjects) {
				obj.y+=velocity;
				if (obj.y < obj.startY)
					obj.y+=obj.startY-obj.y;
		}
	}
	public static void addObject(int x, int y, int width, int height, int phase) {
		addObject(new Platform(x, y, width, height, phase));
	}
	public static void addObject(Platform p) {
		envObjects.add(p);
	}
	public static boolean removeObject(int x, int y, int width, int height) {
		for (Platform obj : envObjects) {
			if (obj.x == x && obj.y == y && obj.width == width && obj.height == height) {
				envObjects.remove(obj);
				return true;
			}
		}
		return false;
	}
	public static boolean removeObject(Platform p) {
		return envObjects.remove(p);
	}
	public static boolean intersects(int x, int y, int width, int height) {
		for (Platform p : envObjects) 
			if (p.intersects(x, y, width, height)) return true;
				return false;
	}
}
