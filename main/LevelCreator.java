package main;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import objects.Platform;

public class LevelCreator implements MouseListener {

	public static ArrayList<Platform> platforms_lc = new ArrayList<Platform>();

	private static int lc, lcX, lcY, lcWidth, lcHeight;

	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3) {
			for (int i=0; i<platforms_lc.size(); i++) {
				final Platform pl = platforms_lc.get(i);
				if (pl.intersects(e.getX(), e.getY(), 1, 1)) {
					platforms_lc.remove(pl);
					i--;
				}
			}
			return;	
		}
		if (lc++ == 0) {
			lcX = e.getX();
			lcY = e.getY();
		} else {
			lc = 0;
			if (e.getX() > lcX) {
				lcWidth = e.getX()-lcX;
			} else if (e.getX() < lcX) {
				final int tempX = lcX;
				lcX = e.getX();
				lcWidth = tempX-e.getX();
			} else return;
			if (e.getY() > lcY) {
				lcHeight = e.getY()-lcY;
			} else if (e.getY() < lcY) {
				final int tempY = lcY;
				lcY = e.getY();
				lcHeight = tempY-e.getY();
			} else return;

			platforms_lc.add(new Platform(lcX, lcY, lcWidth, lcHeight, 0));
		}
	}
	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}

}
