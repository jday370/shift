package main;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import misc.Constants;
import objects.Baddie;
import objects.FinishPortal;
import objects.Platform;
import objects.Portal;
import objects.Shifter;
import environment.Environment;

public class JDGame extends Canvas implements Runnable, KeyListener {
	private static final long serialVersionUID = 1L;

	private boolean running = false;
	private static final int TICK_SPEED = 30;

	private Image image;
	private Graphics dbGraphics;
	public static Color background = null, foreground = null;

	private static Shifter shift;

	public static Platform[] platforms;
	public static Portal[] portals;
	public static Baddie[] baddies;

	public static byte level = 1, phase = 0;

	JDGame() {
		setSize(800, 600);

		addKeyListener(this);
		if (Constants.devMode)
			addMouseListener(new LevelCreator());
	}
	public void startGame() {
		running = true;
		new Thread(this).start();
	}
	public void stopGame() {
		running = false;
	}
	public void run() {
		init();
		try {
			int count = 0;
			while (running) {
				tick(30, count);
				// add more ticks
				count++;
				repaint();
				Thread.sleep(TICK_SPEED);
			}
		} catch (Exception e) {}
	}
	private void init() {
		// Add Ground

		shift = new Shifter();
		newLevel();
		setPhase(0);
	}
	private void tick(final int speed, final int count) throws Exception {
		if (speed % TICK_SPEED != 0) throw new Exception ("Tick speed out of bounds");
		if (count % (speed / TICK_SPEED) != 0) return;

		switch (speed) {
		case 30:		
			shift.tick();
			if (baddies != null)
				for (Baddie bad : baddies) { bad.tick(); }
			break;
		}
	}
	public static void newLevel() {
		switch (level++) {
		case 1:
			platforms = new Platform[] { 
					new Platform(0, Constants.getHeight()-20, Constants.getWidth(), 20, -1),
					new Platform(200, Constants.getHeight()-Shifter.getMaxJumpHeight()*4+40, 25, Constants.getHeight()+180, 0),
					new Platform(0, Constants.getHeight()-Shifter.getMaxJumpHeight(), 200-Shifter.WIDTH*2, 20, 0),
					new Platform(Shifter.WIDTH*2, Constants.getHeight()-Shifter.getMaxJumpHeight()*2+20, 200-Shifter.WIDTH*2, 20, 0),
					new Platform(0, Constants.getHeight()-Shifter.getMaxJumpHeight()*3+40, 200-Shifter.WIDTH*2, 20, 0),
					new Platform(Shifter.WIDTH*2, Constants.getHeight()-Shifter.getMaxJumpHeight()*4+40, 200-Shifter.WIDTH*2, 20, 0),
					new Platform(0, Constants.getHeight()-Shifter.getMaxJumpHeight()*4-Shifter.HEIGHT*2, Constants.getWidth(), 20, 0)
			};
			portals = new Portal[] {
					new FinishPortal(Constants.getWidth()-FinishPortal.DIAMETER-20, Constants.getHeight()-20-FinishPortal.DIAMETER-10, 0)
			};
			break;
		}
	}
	//	private void phaseShift() {
	//		if (++phase == 2)
	//			phase = 0;
	//	}
	private void setPhase(int i) {
		phase = (byte)i;
		switch (i) {
		case 0:
			background = Color.white;
			foreground = Color.black;
			break;
		case 1:
			background = Color.black;
			foreground = Color.white;
			break;
		}
	}
	public void update(final Graphics g) {
		if (image == null) {
			image = createImage(getWidth(), getHeight());
			dbGraphics = image.getGraphics();
		}
		dbGraphics.setColor(background);
		dbGraphics.fillRect(0, 0, getWidth(), getHeight());

		dbGraphics.setColor(foreground);
		paint(dbGraphics);
		g.drawImage(image, 0, 0, this);
	}
	public void paint(Graphics g) {
		if (platforms != null)
			for (Platform pl : platforms) {
				pl.paint(g);
			}
		if (portals != null)
			for (Portal port : portals) {
				port.paint(g);
			}
		if (Constants.devMode)
			for (Platform pl : LevelCreator.platforms_lc) {
				pl.paint(g);
			}
		if (baddies != null)
			for (Baddie bad : baddies) {
				bad.paint(g);
			}
		shift.paint(g);
		if (Constants.devMode)
			g.drawString("Dev Mode Activated.", 5, 15);
	}
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_SPACE:
			if (shift.inPortal != null && shift.inPortal.open) {
				shift.inPortal.teleport(shift);
			}
			break;
		case KeyEvent.VK_UP:
		case KeyEvent.VK_W:
			if (shift.inPortal != null) {
				shift.inPortal.open = true;
			} else {
				shift.jump();
			}
			break;
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_A:
			shift.left = true;
			break;
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_D:
			shift.right = true;
			break;
		case KeyEvent.VK_S:		
			if (shift.inPortal != null) {
				shift.inPortal.open = false;
			} else {
				shift.down = true;
			}
			break;
		case KeyEvent.VK_SHIFT:
			if (level > 2)
				setPhase(1);
			break;

			// Level Creator
		case KeyEvent.VK_BACK_QUOTE:
			for (int i=0; i<LevelCreator.platforms_lc.size(); i++) {
				final Platform pl = LevelCreator.platforms_lc.get(i);
				System.out.println(i + " " + pl.x + " " + pl.y + " " + pl.width + " " + pl.height);
			}
			break;
		case KeyEvent.VK_R:
			for (Platform pl : LevelCreator.platforms_lc) {
				Environment.removeObject(pl);
			}
			LevelCreator.platforms_lc.clear();
			break;
		}
	}
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_A:
			shift.left = false;
			break;
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_D:
			shift.right = false;
			break;
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_S:
			shift.down = false;
			break;
		case KeyEvent.VK_SHIFT:
			setPhase(0); // prev phase?
			break;
		}
	}
	public void keyTyped(KeyEvent e) {}
}
