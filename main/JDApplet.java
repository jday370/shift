package main;

import java.applet.Applet;
import java.awt.BorderLayout;

public class JDApplet extends Applet {

	private static final long serialVersionUID = 1L;
	
	private JDGame game;
	
	public void init() {
		setLayout(new BorderLayout());
		game = new JDGame();
		add(game, BorderLayout.CENTER);
	}
	public void start() {
		game.startGame();
	}
	public void stop() {
		game.stopGame();
	}
}
