package main;

import java.awt.AWTEvent;

import java.awt.Frame;
import java.awt.event.WindowEvent;

import static misc.Constants.*;

public class JDApplication extends Frame {

	private static final long serialVersionUID = 1L;
	
	public static void main(String[] args) {
		new JDApplication();
	}
	private JDApplication() {
		super("Shift v" + VERSION);

		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		setResizable(false);

		final JDGame game = new JDGame();
		add(game);
		game.startGame();

		pack();
		setVisible(true);
	}
	public void processWindowEvent(WindowEvent e) {
		if (e.getID() == WindowEvent.WINDOW_CLOSING) System.exit(0);
	}
}