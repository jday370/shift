package objects;

import java.awt.Graphics;
import java.awt.Rectangle;

import main.JDGame;
import environment.Environment;

public class Platform extends Rectangle {
	/**
	 * Platform for jumping on.
	 * Extended rectangle for phase-shifting functionality.
	 */
	private static final long serialVersionUID = 1L;

	protected int phase;
	public int startY;
	
	public boolean tangible = true;

	public Platform(int x, int y, int width, int height, int phase) {
		super(x, y, width, height);
		this.phase = phase;
		startY = y;

		Environment.addObject(this);
	}
	public boolean isActive() {
		return isActive(JDGame.phase);
	}
	public boolean isActive(int phase) {
		if (this.phase == -1) return true;
		return this.phase == phase;
	}
	public void paint(Graphics g) {
		if (isActive())
			g.fillRect(x, y, width, height);
	}
	public boolean intersects(int x, int y, int width, int height) {
		return isActive() && tangible && ((Rectangle)this).intersects(x, y, width, height);
	}
}
