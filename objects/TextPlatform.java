package objects;

import java.awt.FontMetrics;
import java.awt.Graphics;

import main.JDGame;

public class TextPlatform extends Platform {
	private static final long serialVersionUID = 1L;
	
	public String text;
	
	public TextPlatform(String text, int x, int y, int phase) {
		super(x, y, 0, 0, phase);
		this.text = text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void paint(Graphics g) {
		if (isActive()) {
			final FontMetrics fm = g.getFontMetrics();
			width = fm.stringWidth(text)+10;
			height = fm.getHeight()+5;
			g.fillRect(x, y, width, height);
			fm.stringWidth(text);
			g.setColor(JDGame.background);
			g.drawString(text, x+fm.stringWidth(text)/2, y+fm.getHeight()/2);
			g.setColor(JDGame.foreground);
		}
	}
}
