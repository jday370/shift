package objects;

import static misc.Constants.getHeight;
import static misc.Constants.getWidth;

import java.awt.Graphics;

import main.JDGame;

import environment.Environment;

public class Shifter {
	public double x, speed = 5.2;
	public double y, yvelocity = -20;
	public static final int MAX_VELOCITY_Y = 40;

	public static final int WIDTH = 25, HEIGHT = 40;

	public boolean left, right, jumping, down;

	public Portal inPortal;

	public Shifter() {
		x = 20;
		y = getHeight()-HEIGHT-20;
	}
	public void tick() {
		if (left && (inPortal == null || !inPortal.open)) {
				moveLeft();
		}
		if (right && (inPortal == null || !inPortal.open)) {
				moveRight();
		}
		if (down) {
			panDown();
		}
		if (jumping) {
			if (inPortal != null && inPortal.open && yvelocity < 0) {
				x = inPortal.x+Portal.RADIUS;
				y = inPortal.y-HEIGHT;
				yvelocity += 10;
			}
			moveY();
		}
		if (!Environment.intersects(getX(), getY()+1, WIDTH, HEIGHT)) {
			jumping = true;
		}
		// TODO perfect panning -- left,right,up,down using better algorithm :/
		if (yvelocity > 0 && getY()+HEIGHT >= getHeight()-50 && JDGame.platforms[0].y > JDGame.platforms[0].startY) {
			y = getHeight()-50-HEIGHT;
			Environment.moveAllY(-(int)yvelocity);
		}
		handlePortals();
	}
	private void handlePortals() {
		boolean touchingPortal = false;
		for (Portal port : JDGame.portals) {
			if (port.intersects(x, y, WIDTH, HEIGHT)) {
				touchingPortal = true;
				inPortal = port;
			}
		}
		if (!touchingPortal && inPortal != null) {
			inPortal.open = false;
			inPortal = null;
		}
	}
	public void panDown() {
		if (getY() > 50 && JDGame.platforms[0].y > JDGame.platforms[0].startY) {
			y-=4;
			Environment.moveAllY(-4);
		}
	}
	public void jump() {
		if (Environment.intersects(getX(), getY()+1, WIDTH, HEIGHT)) {
			yvelocity = -20;
			jumping = true;
		}
	}
	public void moveY() { // Note: falling is part of jumping
		if (yvelocity != 0)
			for (Platform pl : Environment.envObjects) {
				if (pl.intersects(getX(), (int)Math.round(y+yvelocity), WIDTH, HEIGHT)) {
					if (yvelocity > 0) yvelocity -= (y+yvelocity+HEIGHT)-(pl.y);
					else if (yvelocity < 0) yvelocity += (pl.y+pl.height)-(y+yvelocity);
				}
			}
		if (Math.round(y+yvelocity) < 50) {
			y = 50;
			Environment.moveAllY((int)Math.abs(yvelocity));
			yvelocity += Environment.GRAVITY;
			return;
		}
		y += yvelocity;
		if (Math.abs(yvelocity) > MAX_VELOCITY_Y)
			yvelocity = MAX_VELOCITY_Y;
		else if (Math.abs(yvelocity) < MAX_VELOCITY_Y)
			yvelocity += Environment.GRAVITY;
	}
	private void moveLeft() {
		int lSpeed = (int) speed;
		for (Platform pl : Environment.envObjects) {
			if (pl.intersects((int)Math.round(x-lSpeed), getY(), WIDTH, HEIGHT-1)) {
				if (x <= pl.x+pl.width) return;
				lSpeed = (int)(speed-(pl.x+pl.width-(x-lSpeed)));
			}
		}
		if (x-speed < 0) lSpeed = (int)(speed+(x-speed));
		x-=lSpeed;
	}
	private void moveRight() {
		int rSpeed = (int) speed;
		for (Platform pl : Environment.envObjects) {
			if (pl.intersects((int)Math.round(x+rSpeed), getY(), WIDTH, HEIGHT-1)) {
				if (x+WIDTH > pl.x) return;
				rSpeed = (int)(rSpeed-(x+WIDTH+rSpeed-pl.x));
			}
		}
		if (x+speed > getWidth()-WIDTH) rSpeed = (int)(getWidth()-WIDTH-x-1);
		x+=rSpeed;
	}
	public void paint(Graphics g) {
		if (inPortal == null || !inPortal.open) {
			g.drawRect(getX(), getY(), WIDTH, HEIGHT);
		}
	}
	public int getX() {
		return (int)Math.round(x);
	}
	public int getY() {
		return (int)Math.round(y);
	}
	public static int getMaxJumpHeight() {
		// sum of arithmetic series (change in displacement 1/2at^2+vit is the same)
		// S = (n/2) � (2a + (n-1)d)
		// n=20/GRAVITY
		// a=-20
		// d=GRAVITY
		final int n = (int) (20/Environment.GRAVITY);
		return -(int)((n/2)*(-40+(n-1)*Environment.GRAVITY));
	}
}