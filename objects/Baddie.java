package objects;

import java.awt.Color;
import java.awt.Graphics;

import main.JDGame;
import environment.Environment;

public class Baddie extends Platform {
	
	private static final long serialVersionUID = 7591320477343743301L;
	
	public double x, speed = 5.5;
	public double y, yvelocity = -20;
	public static final int MAX_VELOCITY_Y = 40;

	public static final int WIDTH = 25, HEIGHT = 40;

	public boolean left, right, jumping;

	private Shifter target;

	public Baddie(int x, int y, Shifter s) {
		super(x, y, WIDTH, HEIGHT, 0);

		this.x = x;
		this.y = y;
		this.target = s;
	}
	public void tick() {
		if (left) {
			moveLeft();
		}
		if (right) {
			moveRight();
		}
		if (jumping) {
			moveY();
		}
		if (!Environment.intersects(roundX(), roundY()+1, WIDTH, HEIGHT)) {
			jumping = true;
		}
		doLogic();
	}
	private void doLogic() {
		// AI for Baddies

		// TODO if hitting something, shift
		// shift for max 2 seconds

		if (x > target.x) left = true;
		else left = false;

		if (x < target.x) right = true;
		else right = false;

		if (y > target.y) jump();
		else left = true;
	}
	private void shift() {
		if (++phase == 2) phase = 0;
	}
	public void jump() {
		if (Environment.intersects(roundX(), roundY()+1, WIDTH, HEIGHT)) {
			yvelocity = -20;
			jumping = true;
		}
	}
	public void moveY() {
		if (yvelocity != 0)
			for (Platform pl : Environment.envObjects) {
				if (pl.intersects(roundX(), (int)Math.round(y+yvelocity), WIDTH, HEIGHT)) {
					if (yvelocity > 0) yvelocity -= (y+yvelocity+HEIGHT)-(pl.y);
					else if (yvelocity < 0) yvelocity += (pl.y+pl.height)-(y+yvelocity);
					System.out.println(pl.y+pl.height-y+yvelocity);
				}
			}
		y += yvelocity;
		if (Math.abs(yvelocity) > MAX_VELOCITY_Y)
			yvelocity = MAX_VELOCITY_Y;
		else if (Math.abs(yvelocity) < MAX_VELOCITY_Y)
			yvelocity += Environment.GRAVITY;
	}
	private void moveLeft() {
		int lSpeed = (int) speed;
		for (Platform pl : Environment.envObjects) {
			if (pl.intersects((int)Math.round(x-lSpeed), roundY(), WIDTH, HEIGHT-1)) {
				if (x <= pl.x+pl.width) return;
				lSpeed = (int)(speed-(pl.x+pl.width-(x-lSpeed)));
			}
		}
		if (x-speed < 0) lSpeed = (int)(speed+(x-speed));
		x-=lSpeed;
	}
	private void moveRight() {
		int rSpeed = (int) speed;
		for (Platform pl : Environment.envObjects) {
			if (pl.intersects((int)Math.round(x+rSpeed), roundY(), WIDTH, HEIGHT-1)) {
				if (x+WIDTH > pl.x) return;
				rSpeed = (int)(rSpeed-(x+WIDTH+rSpeed-pl.x));
			}
		}
		if (x+speed > getWidth()-WIDTH) rSpeed = (int)(getWidth()-WIDTH-x-1);
		x+=rSpeed;
	}
	public void paint(Graphics g) {
		if (isActive()) {
			g.setColor(Color.red);
			g.fillRect(roundX(), roundY(), WIDTH, HEIGHT);
			g.setColor(JDGame.foreground);
			g.drawRect(roundX(), roundY(), WIDTH, HEIGHT);
		}
	}
	public int roundX() {
		return (int)Math.round(x);
	}
	public int roundY() {
		return (int)Math.round(y);
	}
}
