package objects;

import java.awt.Graphics;

import main.JDGame;

public class Portal extends Platform {

	private static final long serialVersionUID = 1L;
	
	public static final int RADIUS = 20, DIAMETER = RADIUS*2;
	
	public boolean open;
	
	public int toX, toY;
	
	public Portal(int x, int y, int toX, int toY, int phase) {
		super(x, y, RADIUS*2, RADIUS*2, phase);
		this.toX = toX;
		this.toY = toY;
		tangible = false;
	}
	public void teleport(Shifter s) {
		s.x = toX+RADIUS;
		s.y = toY;
	}
	private void paintOpen(Graphics g) {
		g.setColor(JDGame.foreground);
		g.fillOval(x, y, width, height);
		g.setColor(JDGame.background);
		g.fillOval(x+2, y+2, width-4, height-4);
		g.setColor(JDGame.foreground);
		g.fillOval(x+4, y+4, width-8, height-8);
	}	
	private void paintClosed(Graphics g) {
		g.setColor(JDGame.foreground);
		g.fillOval(x, y, width, height);
		g.setColor(JDGame.background);
		g.fillOval(x+2, y+2, width-4, height-4);
		g.setColor(JDGame.background);
		g.fillOval(x+10, y+10, width-20, height-20);
		g.setColor(JDGame.foreground);
		g.drawLine(x+RADIUS, y+1, x+RADIUS, y+DIAMETER-1);
		g.fillOval(x+RADIUS-9, y+RADIUS-1, 5, 5);
		g.fillOval(x+RADIUS+4, y+RADIUS-1, 5, 5);
	}
	public void paint(Graphics g) {
		if (isActive()) {
			if (open) paintOpen(g);
			else paintClosed(g);
			
			g.setColor(JDGame.foreground);
		}
	}
}
