package objects;

import main.JDGame;
import misc.Constants;

public class FinishPortal extends Portal {

	private static final long serialVersionUID = 1L;
	
	public boolean open = false;
	
	public FinishPortal(int x, int y, int phase) {
		super(x, y, 0, 0, phase);
	}
	public void teleport(Shifter s) {
		s.x = 20;
		s.y = Constants.getHeight()-Shifter.HEIGHT-20;
		JDGame.newLevel();
	}
}
