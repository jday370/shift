package misc;

public class Constants {
	public static final double VERSION = 1.0;
	
	public static final boolean devMode = false;
	
	public static final int getWidth() { return 800; }
	public static final int getHeight() { return 600; }
}
